
package poo;

public abstract class ContaBancaria {

  
    
    private long numero;
    private double saldo;
  
    
   
    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

   
    //Construtor
    public ContaBancaria(long numero, double saldo) {
        this.numero = numero;
        this.saldo = saldo;
        
    }

    public void sacar(double valor){
        saldo -= valor;
        //saldo = saldo - valor;
    }
    
    public void depositar(double valor){
        saldo += valor;
        //saldo = saldo + valor;
    }
    
    
}
