package poo;

public class ContaCorrente extends ContaBancaria{

	private double TaxaOperacao ;
	
	public double getTaxaOperacao() {
		return TaxaOperacao;
	}

	public void setTaxaOperacao(double taxaOperacao) {
		TaxaOperacao = taxaOperacao;
	}
	
	@Override
	public void sacar(double valor){
        super.setSaldo(super.getSaldo() - valor*TaxaOperacao);
        //saldo = saldo - valor multiplicado pela taxa;
    }
	
	@Override
	public void depositar(double valor) {
		
		super.setSaldo(super.getSaldo() + valor - (valor*TaxaOperacao));
		// saldo = saldo + valor menos valor multiplicado pela taxa 
	}
	
	public ContaCorrente(long numero, double saldo, double TaxaOperacao) {
		super(numero, saldo);
        this.TaxaOperacao = TaxaOperacao;
		
	}

	
}
