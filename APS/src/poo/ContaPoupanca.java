package poo;

public class ContaPoupanca extends ContaBancaria {
	
	private double limite = super.getSaldo() + 1500;
	
	@Override
	public void sacar(double valor){ 
		if(valor <= limite) {
        super.setSaldo(super.getSaldo() - valor);
        //saldo = saldo - valor
    }}
	
	@Override
	public void depositar(double valor) {
		
		super.setSaldo(super.getSaldo() + valor );
		// saldo = saldo + valor 
	}
	
	public ContaPoupanca(long numero, double saldo , double limite) {
		super(numero, saldo);
		this.limite = limite;
		
		
	}

}
